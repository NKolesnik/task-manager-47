package ru.t1consulting.nkolesnik.tm.exception.field;

public final class DateIncorrectException extends AbstractFieldException {

    public DateIncorrectException() {
        super("Error! Date incorrect...");
    }

}
package ru.t1consulting.nkolesnik.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    boolean existsById(@Nullable String id);

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@Nullable String id);

    void remove(@Nullable M model);

    void removeById(@Nullable String id);

    void update(@NotNull M model);

    void clear();

    long getSize();

}

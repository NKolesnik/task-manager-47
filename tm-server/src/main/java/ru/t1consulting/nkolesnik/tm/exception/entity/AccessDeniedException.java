package ru.t1consulting.nkolesnik.tm.exception.entity;

public final class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
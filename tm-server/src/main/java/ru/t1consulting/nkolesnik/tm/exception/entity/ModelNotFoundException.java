package ru.t1consulting.nkolesnik.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}
package ru.t1consulting.nkolesnik.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IUserOwnedRepository;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;
import ru.t1consulting.nkolesnik.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@Nullable String userId, @Nullable M model) {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final User user = entityManager
                .createQuery("FROM User WHERE id = :id", User.class)
                .setParameter("id", userId)
                .getSingleResult();
        if (user == null) return;
        model.setUser(user);
        entityManager.persist(model);
    }

    @Override
    public abstract long getSize(@Nullable String userId);

    @NotNull
    @Override
    public abstract List<M> findAll(@Nullable String userId);

    @Nullable
    @Override
    public abstract M findById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract void clear(@Nullable String userId);

    @Override
    public abstract void remove(@Nullable String userId, @Nullable M model);

    @Override
    public abstract void removeById(@Nullable String userId, @Nullable String id);

}

package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.NameComparator;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@Nullable M modelDTO) {
        entityManager.persist(modelDTO);
    }

    @Override
    public abstract long getSize();

    @NotNull
    @Override
    public abstract List<M> findAll();

    @Nullable
    @Override
    public abstract M findById(@Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String id);

    @Override
    public void update(@NotNull M modelDTO) {
        entityManager.merge(modelDTO);
    }

    @Override
    public abstract void clear();

    @Override
    public abstract void remove(@Nullable M modelDTO);

    @Override
    public abstract void removeById(@Nullable String id);

    protected String getSortColumnName(@Nullable Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == DateBeginComparator.INSTANCE) return "dateBegin";
        else return "status";
    }

}

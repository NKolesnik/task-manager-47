package ru.t1consulting.nkolesnik.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskRemoveByIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}

package ru.t1consulting.nkolesnik.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getName();

    @Nullable
    String getDescription();

    @Nullable
    String getArgument();

}

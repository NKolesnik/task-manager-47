package ru.t1consulting.nkolesnik.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskChangeStatusByIdResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = getTaskEndpoint().changeTaskStatusById(request);
        @Nullable TaskDTO task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
